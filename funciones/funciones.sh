#!/bin/bash
#identificar origen y destino
function llamarND
{
	if [ "$1" == "inc" ];
	then
		numerocaracteres=$(echo -n "$opcion" | wc -c)
		menos=$(($numerocaracteres - 5))
		nombre=`echo "$opcion" | cut  -c 1-$menos`
		destino;
		generarnombre $1;
	else
		nombre;
		destino;
		generarnombre $1;
	fi
}

function destino
{
	destino=../copias/
}
#entrada de parametro nombre
function nombre
{
	nombreintroducido=$( zenity --entry --text "Nombre")
	if [ "$?" = 1 ]; 
	then 
		zenity --error --title="ERROR!" --text="Copia cancelada"
		./m_principal.sh
		exit
	fi
	if [ "$?" = 0 ];
	then
		if [ -z "$nombreintroducido" ];
		then
			zenity --notification --window-icon="info" --text="Necesita poner un nombre."
			errorsys
			exit
		else
			nombre=`echo $nombreintroducido | sed 's/^[[:space:]]*//' | sed 's/[[:space:]]*//' | tr " " '_'`
		fi
	fi
	
}
#creacion de nombre segun las epecificaciones
function generarnombre
{
	#error de espacio en blanco buscar solucion
	nombreconfecha=`date +%Y_%m_%d_%T`-$nombre
	nombreArchivo=$nombreconfecha.$1.tar.gz
}
function generartarCI
{
	echo -n tar -zcvf $1$2 -g ../metadatos/$4 > cop_temp.sh
	echo -n " " >> cop_temp.sh
	echo  \"$3\" >> cop_temp.sh
}
#Creacion de la copia completa
function completa
{
	(
	echo "10";sleep 1
	echo "# Preparando copia";sleep1
	echo "20";sleep 1
	echo "# Iniciando copia";sleep1
	#tar -cpvzf $destino/$nombreArchivo -g ../metadatos/$nombre.snap $origen > ../log/logdearchivos/$nombreArchivo.log
	generartarCI $destino $nombreArchivo "$origen" "$nombre.snap"
	echo "50";sleep 1
	echo "# Copiando..."
	./cop_temp.sh > ../log/logdearchivos/$nombreArchivo.log
	#rm ./cop_temp.sh necesitamos que la carpeta siempre tenga los permisos
	echo "70";sleep 1
	echo "# Finalizando copia..."
	ls ../copias/ > ../log/copiasactuales.log
	echo "100"
	echo "# Copia finalizada..."
	echo ------------------------------------------------------------------------------------------------------ >> ../log/copiasrealizadas.log
	echo $nombreArchivo >> ../log/copiasrealizadas.log
	echo contenia $origen >> ../log/copiasrealizadas.log

	) | zenity --progress \
		title="Progreso" \
		text="Realizando copia..." \
		percentage=0
	if [ "$?" = 1 ]
	then
		$(zenity --error --title="ERROR!" --text="Copia cancelada")
		[ -f ../copias/$nombreArchivo ] && rm ../copias/$nombreArchivo
		[ -f ../metadatos/$nombre.snap ] && rm ../metadatos/$nombre.snap
		[ -f ../log/logdearchivos/$nombreArchivo.log ] && rm ../log/logdearchivos/$nombreArchivo.log
		if [ "$?" = 0 ]
		then
			./m_principal.sh
			exit
		fi
	fi
	if [ "$?" = 0 ]
	then
		volver=$(zenity --question --text "¿ Volver al menu principal ?" --ok-label="Si" --cancel-label="No")
		case $? in
		0)
			./m_principal.sh
		;;
		1)
			exit
		;;
		-1)
			errorsys
		;;
		esac
	fi

}
function incremental
{
	(
	echo "10";sleep 1
	echo "# Preparando copia";sleep1
	echo "20";sleep 1
	echo "# Iniciando copia";sleep1
	generartarCI $destino $nombreArchivo "$origen" $opcion
	echo "50";sleep 1
	echo "# Copiando..."
	./cop_temp.sh > ../log/logdearchivos/$nombreArchivo.log
	echo "70";sleep 1
	echo "# Finalizando copia..."
	ls ../copias/ > ../log/copiasactuales.log
	echo "100"
	echo "# Copia finalizada..."

	) | zenity --progress \
		title="Progreso" \
		text="Realizando copia..." \
		percentage=0

	if [ "$?" = -1 ]; then 
	zenity --error \
	errorsys;
	fi

	if [ "$?" = 0 ];then 
		volver=$(zenity --question --text "¿ Volver al menu principal ?" --ok-label="Si" --cancel-label="No")
		if [ "$?" = 0 ]
		then 
			./m_principal.sh
		else	
			exit
		fi
	fi
}
function menuincremental
{
	#busco todo lo acabado como .snap en la carpeta metadatos y lo metemos en un archivo

	`find ../metadatos/ -name "*.snap" > ../log/incrementalmetadatos.log`

	#contamos todo el contenido de incrementalmetadatos.log

	$(wc -l ../log/incrementalmetadatos.log > ../log/contar.log )

	#al hacer arriba un contar no saldra por lineas el numero entonces cogemos el primer caracter de la ultima linea

	contador=$(cat ../log/contar.log | cut -c1-2)

	#quitamos los saltos de linea
	restauracion=$(cat ../log/incrementalmetadatos.log | tr '\n' ' ')

	#hacemos un if contador y si es igual a 1 cogera el resultado de incrementalmetadatos.log en una variable que despues cogeremos solamente el nombre del archivo mediante el uso de basename despues lo gurdaremos en opciones

	if [ "$contador" -eq "1" ];
		then
			valor=`cat ../log/incrementalmetadatos.log`
			basename "$valor" >> ../log/opciones

	#si hay mas de un resultado en contador deberemos hacer un array y ir guardando uno a uno en un archivo opciones

		else
			IFS=' ' read -r -a array <<< "$restauracion"
			for element in "${array[@]}"
			do
			#meto el array en un archivo, elemento a elemento
			basename "$element" >> ../log/opciones
		done
	fi

	#quito los saltos de linea del archivo con el siguiente comando

	if [ "$contador" -ne "1" ];
		then
			valorf=$(cat ../log/opciones | tr '\n' ' ')
		else
			valorf=$(<  ../log/opciones)
	fi
}
function errorsys
{
	zenity --error \ --text="Huvo un error al intentar realizar la operacion."
	if [ "$?" = 0 ];then 
		volver=$(zenity --question --text "¿ Volver al menu principal ?" --ok-label="Si" --cancel-label="No")
		if [ "$?" = 0 ]
		then 
			./m_principal.sh
		else	
			exit
		fi
	fi
}
#Hernando comprueba la exitencia y creacion el path para llamar a copytuxpor shell
function instalar
{
	#compruebar si tienes privilegios

	if [ $UID == 0 ]
		then
		(
			# proceso de carga y creacion de directorio en la raiz 
			echo "10" ; sleep 1
			echo "# Comprobando diectorio" ; sleep 1
			if [ !-d /copytux ]
			then
			echo "20" ; sleep 1
			echo "# Creando directorios" ; sleep 1
				mkdir  /copytux
			else
			echo 'ya existe el directorio  desesa borralo'
			zenity -- question --title="ya existe el directorio  desesa borralo" --text="Borra directorio para crear copytux"
			
			fi
			
			echo "50" ; sleep 1
			echo "Moviendo archivos" ; sleep 1
			echo "75" ; sleep 1
			echo "#Registrando enel sistema CopyTux" ; sleep 1
			echo "100" ; sleep 1
		) |
			zenity --progress \
			title="Instalado copytux" \
			text="Rastreando los registros de los correos..." \
			percentage=0
			# cancelar
		if [ "$?" = 1 ]
			then
			zenity --error \
			text="Instalacion cancelada"
		else
			then

		fi
	#si no los tiene  da un mensaje de error y pide para su ejecucion  	
	fi


}
#Hernando menu de inicio del shell.app y llamada a la restauracion y administracion
function inicio
{
	. ../m_principal.sh
}








