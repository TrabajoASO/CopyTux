#!/bin/bash
# Debe copiar el archivo "COPYING" en la misma carpeta que este script.
. funciones/funciones.sh
FILE=`dirname $0`/Licencia/EULA

zenity --text-info \
       --title="Licencia" \
       --filename=$FILE \
       --checkbox="He leído y acepto los términos."

case $? in
    0)
        echo "Comenzar instalación"
        instalar
	;;
    1)
        echo "Detener instalación"
	;;
    -1)
        echo "Ha ocurrido un error inesperado."
	;;
esac