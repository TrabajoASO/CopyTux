#!/bin/bash
. ../funciones/funciones.sh

if [ -f "../log/opciones" ]; then rm ../log/opciones; fi
if [ -f "../log/incrementalmetadatos.log" ]; then rm ../log/incrementalmetadatos.log; fi
if [ -f "../log/contar.log" ]; then rm ../log/contar.log; fi

menuincremental
opcion=$(zenity --width=500 --height=500 --list --title "Menu copias" --text "Que copia desea realizar" --column "Opciones" $valorf)
if [ "$?" = 0 ]
then
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar la copia completa que se realizo."
		./m_incrementalD.sh
		exit
	else
		origen=`zenity --file-selection --directory --multiple --title "Seleccione un archivo" --separator='" "'`
		case $? in
			0)
				llamarND inc && incremental $opcion;;
			1)
				copias.sh;;
			-1)
				errorsys;;
		esac
	fi
else
	./m_principal.sh
fi





