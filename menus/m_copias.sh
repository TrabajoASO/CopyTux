#!/bin/bash
opcion=$(zenity --width=350 --height=300 --list --title "Menu copias" --text "¿ Que tipo de copia desea realizar ?" --radiolist --column "" --column "Opciones" FALSE "Completa" FALSE "Diferencial" FALSE "Incremental" --ok-label="Siguiente" --cancel-label="Volver")
if [ "$?" = 1 ]
then
	./m_principal.sh
	exit
fi
if [ "$?" = 0 ]
then
	#mira si la variable esta vacia si es el caso volvera atras
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar una de las opciones para poder continuar."
		./m_copias.sh
		exit
	else
		case $opcion in
		"Completa" ) ./m_completa.sh;;
		"Diferencial" ) ./m_restaurar.sh;;
		"Incremental" ) ./m_incrementalD.sh;;
		esac
	fi
fi



