#!/bin/bash
opcion=$(zenity --width=350 --height=300 --list --title "CopyTux simplemente rapido y sencillo" --text "¿ Que desea hacer ?" --radiolist --column "" --column "Opciones" FALSE "Hacer copia" FALSE "Restaurar copia" FALSE "Administrar" --ok-label="Siguiente" --cancel-label="Salir" )
if [ "$?" = 1 ];
then
	option=$(zenity --question --text="¿Está seguro de que quiere salir?" --ok-label="SI" --cancel-label="NO")
	if [ "$?" = 0 ];
	then
		exit
	else
		./m_principal.sh
		exit
	fi
fi
if [ -z "$opcion" ];
then
	 zenity --notification --window-icon="info" --text="Necesita seleccionar una de las opciones para poder continuar."
	./m_principal.sh
else
	case $opcion in
	"Hacer copia" ) ./m_copias.sh;;
	"Restaurar copia" ) ./m_restaurar.sh;;
	"Administrar" ) ./m_administrar.sh;;
	esac
fi
