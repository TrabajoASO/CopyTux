#!/bin/bash
. ../funciones/funciones.sh
opcion=$(zenity --width=350 --height=300 --list --title "Menu copias" --text "Que tipo de copia desea restaurar" --radiolist --column "" --column "Opciones" FALSE "Completa" FALSE "Diferencial" FALSE "Incremental" --ok-label="Siguiente" --cancel-label="Volver")
if [ "$?" = 1 ]
then
	./m_principal.sh
	exit
fi
if [ "$?" = 0 ]
then
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar una copia para poder restaurar."
		./m_restaurar.sh
		exit
	else
		case $opcion in
		"Completa" ) ./m_restaurarC.sh;;
		"Diferencial" ) ./restaurar.sh;;
		"Incremental" ) ./administrar.sh;;
		esac
	fi
fi
