#!/bin/bash
. ../funciones/funciones.sh
opcion=$(zenity --width=350 --height=300 --list --title "Menu completa" --text "¿ Que copia desea realizar ?" --radiolist --column "" --column "Opciones" FALSE "Elegir uno o varios archivos" FALSE "Elegir una o varias carpetas" --ok-label="Siguiente")
if [ "$?" = 1 ]
then
	./m_copias.sh
	exit
fi

if [ "$?" = 0 ]
then
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar el tipo de copia completa para continuar."
		./m_completa.sh
	else
		case $opcion in
		"Elegir uno o varios archivos" ) ./m_completaVA.sh;;
		"Elegir una o varias carpetas" ) ./m_completaD.sh;;
		esac
	fi
fi

