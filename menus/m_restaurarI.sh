#!/bin/bash
. ../funciones/funciones.sh

if [ -f "../log/opciones" ]; then rm ../log/opciones; fi
if [ -f "../log/incrementalmetadatos.log" ]; then rm ../log/incrementalmetadatos.log; fi
if [ -f "../log/contar.log" ]; then rm ../log/contar.log; fi

`find ../copias/ -name "*.com.tar.gz" > ../log/incrementalmetadatos.log`


$(wc -l ../log/incrementalmetadatos.log > ../log/contar.log )


contador=$(cat ../log/contar.log | cut -c1-2)


restauracion=$(sed -n -e '1x;1!H;${x;s-\n- -gp}' < ../log/incrementalmetadatos.log)


if [ "$contador" -eq "1" ];
	then
		valor=`cat ../log/incrementalmetadatos.log`
		basename "$valor" >> ../log/opciones

	else
		IFS=' ' read -r -a array <<< "$restauracion"
		for element in "${array[@]}"
		do
		basename "$element" >> ../log/opciones
	done
fi

if [ "$contador" -ne "1" ];
	then
		valorf=$(tr -d "\n" < ../log/opciones)
	else
		valorf=$(<  ../log/opciones)
fi

opcion=$(zenity --width=500 --height=500 --list --title "Menu copias" --text "Que copia desea realizar" --column "Opciones" $valorf)

if [ "$?" = 0 ]
then
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar la copia completa que se realizo."
		./m_restaurarI.sh
		exit
	else
		if [ -f "../log/opciones" ]; then rm ../log/opciones; fi
		if [ -f "../log/incrementalmetadatos.log" ]; then rm ../log/incrementalmetadatos.log; fi
		if [ -f "../log/contar.log" ]; then rm ../log/contar.log; fi

		numerocaracteres=$(echo -n "$opcion" | wc -c)
		menos=$(($numerocaracteres - 11))
		nombre=`echo "$opcion" | cut  -c 21-$menos`
		`find ../copias/ -name "*$nombre.inc.tar.gz" > ../log/incrementalmetadatos.log`
		#echo $nombre > nombre.txt
		$(wc -l ../log/incrementalmetadatos.log > ../log/contar.log )
		contador=$(cat ../log/contar.log | cut -c1-2)
		restauracion=$(sed -n -e '1x;1!H;${x;s-\n- -gp}' < ../log/incrementalmetadatos.log)

		if [ "$contador" -eq "1" ];
		then
		valor=`cat ../log/incrementalmetadatos.log`
		prueba=`basename "$valor"`
		echo -n 'TRUE' $prueba >> ../log/opciones
		else
		IFS=' ' read -r -a array <<< "$restauracion"
		for element in "${array[@]}"
		do
		prueba=`basename "$element"`
		echo -n 'TRUE ' >> ../log/opciones
		echo -n  "$prueba" >> ../log/opciones
		echo -n ' ' >> ../log/opciones
		done
		fi

		if [ "$contador" -ne "1" ];
		then
		valorf=$(tr -d "\n" < ../log/opciones)
		else
		valorf=$(< ../log/opciones)
		fi
		#echo -n zenity --width=500 --height=500 --list --title \"Menu copias\" --text \"Que copia desea realizar\" --checklist --column \"\" --column \"Opciones\" > archivofin.txt
		echo -n " " >> archivofin.txt
		echo -n $valorf >> archivofin.txt
		opcion2=$(zenity --width=500 --height=500 --list --title "Menu copias" --text "Que copia desea realizar" --checklist --column "" --column "Opciones" $valorf)

	fi

else
	./m_principal.sh
fi


