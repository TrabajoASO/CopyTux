#!/bin/bash
. ../funciones/funciones.sh

`find ../copias/ -name "*.com.tar.gz" > ../log/restauracionesC.log`
$(wc -l ../log/restauracionesC.log > ../log/contar.log ) 
contador=$(cat ../log/contar.log | cut -c1-2)

#prueba de menus el primer comando o que hace es eliminar los saltos de linea

restauracion=$(cat ../log/opciones | tr '\n' ' ')

#monta el array con la separacion de ' ' en la linea http://stackoverflow.com/questions/10586153/split-string-into-an-array-in-bash

if [ "$contador" -eq "1" ]; 
	then 
	valor=`cat ../log/restauracionesC.log`
	basename "$valor" >> ../log/opciones
	else
	IFS=' ' read -r -a array <<< "$restauracion"
		for element in "${array[@]}"
		do

		#meto el array en un archivo elemento a elemento

		basename "$element" >> ../log/opciones
		done
fi

#quito los saltos de linea del archivo con el siguiente comando

if [ "$contador" -ne "1" ]; 
	then
	valorf=$(cat ../log/opciones | tr '\n' ' ')
else
	valorf=$(<  ../log/opciones)
	echo $valorf
fi

#muestra los valores del archivo

opcion=$(zenity --width=500 --height=500 --list --title "Menu copias" --text "¿ Que copia completa deseas restaurar ?" --column "Opciones" $valorf)
if [ "$?" = 1 ]
then
	rm ../log/opciones
	rm ../log/restauracionesC.log
	rm ../log/contar.log
	./m_restaurar.sh
	exit
fi

if [ "$?" = 0 ]
then
	if [ -z "$opcion" ];
	then
		zenity --notification --window-icon="info" --text="Necesita seleccionar la copia completa a restaurar."
		rm ../log/opciones
		rm ../log/restauracionesC.log
		rm ../log/contar.log
		./m_restaurarC.sh
		exit
	else
		#restauro la copia en la raiz
		$(tar -xzvf ../copias/$opcion -C / >> ../log/restauracionesrealizadas.log)

		#creo el log

		echo "" >> ../log/restauracionesrealizadas.log
		echo "------------------------------------------------------------------------------------" >> ../log/restauracionesrealizadas.log
		echo $opcion "restauracion realizada el dia `date +%Y_%m_%d_%T`">> ../log/restauracionesrealizadas.log
		echo "------------------------------------------------------------------------------------" >> ../log/restauracionesrealizadas.log
		echo "" >> ../log/restauracionesrealizadas.log

		#borro los archivos temporales

		rm ../log/opciones
		rm ../log/restauracionesC.log
		rm ../log/contar.log
	fi
fi







