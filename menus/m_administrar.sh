#!/bin/bash
. ../funciones/funciones.sh

tipo=$(zenity  --list  --text "Administracion" --radiolist)
if [ "$?" = 1 ]
then
	./m_principal.sh
	exit
fi

if [ "$?" = 0 ]
    then
        if [ -z "$tipo" ]
            then
                zenity --notification --window-icon="info" --text="Necesita seleccionar el tipo de copia completa para continuar."
                ./m_completa.sh
            else
                case $opcion in
                "e" ) # Script a llamar ;;
                "E" ) # Script a llamar ;;
                esac
        fi
fi
